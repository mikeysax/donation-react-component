import React from 'react';
import './index.scss';

// Pages
import HomePage from './Home';

export default function App() {
	return (
		<div className="App">
			<HomePage />
		</div>
	);
}
