import React from 'react';
import './index.scss';

// Components
import Donation from '../../components/Donation';

export default function Home() {
	return (
		<div className="Home">
			<Donation
				deadlineDate={'9/30/2020'}
				monetaryGoal={5000}
				minDonation={5}
			/>
			<div className="details">
				Code Available On{' '}
				<a
					href="https://gitlab.com/mikeysax/donation-react-component"
					target="_blank"
					rel="noopener noreferrer"
				>
					Gitlab
				</a>
			</div>
		</div>
	);
}
