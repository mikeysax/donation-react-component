export const validateStrDateFormat = (date) =>
	date.match(/^\d{1,2}\/\d{1,2}\/\d{2,4}$/);

export const formatDateToStrDate = (date) => {
	const month = date.getMonth() + 1;
	const day = date.getDate();
	const fullYear = date.getFullYear();
	return month + '/' + day + '/' + fullYear;
};

export const calculateDaysBetweenDates = (date1, date2) => {
	if (!validateStrDateFormat(date1) || !validateStrDateFormat(date2)) {
		console.error(
			'Date Values Are Not Formatted Correctly As X/X/XX, XX/XX/XX, or XX/XX/XXXX'
		);
		return '';
	}
	const millisecondsInDay = 1000 * 60 * 60 * 24;
	const differenceInMilliseconds = Math.abs(
		new Date(date1) - new Date(date2)
	);
	const remainingDaysLeft = Math.round(
		differenceInMilliseconds / millisecondsInDay
	);
	return remainingDaysLeft;
};
