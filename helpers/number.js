export const addCommaToNumber = (num) =>
	num.toString().replace(/\B(?=(?:\d{3})+(?!\d))/g, ',');

export const calculateSum = (numberArray) =>
	numberArray.reduce((sum, num) => (sum += num), 0);

export const calculateRemaining = (totalVal, currentVal) =>
	totalVal - currentVal;
