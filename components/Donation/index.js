import React, { useState, useEffect } from 'react';
import './index.scss';

// Components
import ProgressBar from '../ProgressBar';
import Bubble from '../Bubble';

// Helpers
import {
	calculateDaysBetweenDates,
	formatDateToStrDate,
} from '../../helpers/dateTime.js';
import { onlyNumbers } from '../../helpers/string.js';
import {
	calculateSum,
	calculateRemaining,
	addCommaToNumber,
} from '../../helpers/number.js';

// Other
import numberToWords from 'number-to-words';

export default function Donation(props) {
	const { deadlineDate, monetaryGoal, minDonation } = props;
	const [state, setState] = useState({
		collectedDonations: [],
		pendingDonation: '',
		message: '',
		isError: false,
	});

	useEffect(() => {
		if (state.message) {
			const timer = setTimeout(() => {
				cleanUpMessage();
			}, 4000);
			return () => clearTimeout(timer);
		}
	});

	const cleanUpMessage = () =>
		setState({
			...state,
			message: '',
		});

	const remainingDaysLeft = () => {
		const numOfDaysLeft = calculateDaysBetweenDates(
			deadlineDate,
			formatDateToStrDate(new Date())
		);
		return numberToWords.toWords(numOfDaysLeft);
	};

	const totalDonors = () => state.collectedDonations.length;

	const validateAndUpdatePendingDonation = (e) => {
		const value = e.target.value;
		if (value === '' || value.match(/^\d+$/)) {
			setState({
				...state,
				pendingDonation: onlyNumbers(value),
				message: '',
				isError: false,
			});
		} else {
			setState({
				...state,
				message: 'Only numbers are allowed in the donation field',
				isError: true,
			});
		}
	};

	const totalDonated = () => calculateSum(state.collectedDonations);

	const remainingDonationNeeded = () => {
		const remaining = calculateRemaining(monetaryGoal, totalDonated());
		return remaining >= 0 ? addCommaToNumber(remaining) : 0;
	};

	const donationMinimumIsMet = (incomingDonation) =>
		incomingDonation >= minDonation;

	const submitDonation = (e) => {
		e.preventDefault();
		const incomingDonation = new Number(state.pendingDonation);
		if (!donationMinimumIsMet(incomingDonation)) {
			return setState({
				...state,
				message: `A donation less than $${minDonation} is not allowed`,
				isError: true,
			});
		}
		setState({
			...state,
			pendingDonation: '',
			collectedDonations: [...state.collectedDonations, incomingDonation],
			message: `Thank you for your donation of $${incomingDonation}!`,
			isError: false,
		});
	};

	const messageClass = () =>
		'message ' + (state.isError ? '-error' : '-success');

	return (
		<div className="Donation">
			<Bubble>
				<div
					className="monetary-value"
					aria-label="remaining donation needed"
				>
					<div className="dollar-symbol">$</div>
					{remainingDonationNeeded()}
				</div>{' '}
				still needed to fund this project
			</Bubble>
			<ProgressBar
				maxValue={monetaryGoal}
				currentValue={totalDonated()}
			/>
			<h1>Only {remainingDaysLeft()} days left to fund this project</h1>
			<p>
				Join the <b>{totalDonors()}</b> other donors who have already
				supported this project.
			</p>
			<form onSubmit={submitDonation}>
				<div className="icon">$</div>
				<label htmlFor="donation">Donation</label>
				<input
					type="text"
					id="donation"
					name="donation"
					value={state.pendingDonation}
					onChange={validateAndUpdatePendingDonation}
				/>
				<button type="submit" aria-label="submit donation">
					Give Now
				</button>
			</form>
			{state.message && (
				<div className={messageClass()} aria-live="polite">
					{state.message}
				</div>
			)}
		</div>
	);
}
