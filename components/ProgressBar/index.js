import React from 'react';
import './index.scss';

export default function ProgressBar(props) {
	const { maxValue, currentValue } = props;

	const calculateProgress = () => (currentValue / maxValue) * 100;

	return (
		<div className="ProgressBar">
			<div
				className="progress"
				style={{ width: calculateProgress() + '%' }}
				aria-label={calculateProgress() + ' percent donated'}
				role="progressbar"
				aria-valuenow={calculateProgress()}
				aria-valuemin="0"
				aria-valuemax="100"
			></div>
		</div>
	);
}
