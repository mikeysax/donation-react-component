import React from 'react';
import './index.scss';

export default function Bubble(props) {
	const { children } = props;
	return <div className="Bubble">{children}</div>;
}
